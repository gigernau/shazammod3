//
//  ViewController.swift
//  ttt
//
//  Created by Gianluca De Lucia on 04/12/2019.
//  Copyright © 2019 Gianluca De Lucia. All rights reserved.
//

import UIKit
import Vision
import SoundAnalysis
import AVFoundation
import CoreML



class ViewController: UIViewController{
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet var imgvAvatar: UIView!
    
    @IBOutlet weak var label: UILabel!


    var model1:MySoundClassifier!
    var pulseLayers = [CAShapeLayer]()
    

    public var text =  ""
    
    // Audio Engine
    var audioEngine = AVAudioEngine()
    
    // Streaming Audio Analyzer
    var streamAnalyzer: SNAudioStreamAnalyzer!
    
    // Serial dispatch queue used to analyze incoming audio buffers.
    let analysisQueue = DispatchQueue(label: "com.apple.AnalysisQueue")

    var resultsObserver: ResultsObserver!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model1 = MySoundClassifier()
        imgvAvatar.layer.cornerRadius = imgvAvatar.frame.size.width/2.0
        let model: MLModel = self.model1.model
        
        // Get the native audio format of the engine's input bus.
        let inputFormat = self.audioEngine.inputNode.inputFormat(forBus: 0)

        // Create a new stream analyzer.
        streamAnalyzer = SNAudioStreamAnalyzer(format: inputFormat)

        // Create a new observer that will be notified of analysis results.
        // Keep a strong reference to this object.
        resultsObserver = ResultsObserver()
        
        // Get the native audio format of the engine's input bus.


        do {
            // Prepare a new request for the trained model.
            let request = try SNClassifySoundRequest(mlModel: model)
            try
                streamAnalyzer.add(request, withObserver: resultsObserver)
                
        } catch {
            print("Unable to prepare request: \(error.localizedDescription)")
            return
        }

        // Install an audio tap on the audio engine's input node.
        self.audioEngine.inputNode.installTap(onBus: 0,
                                         bufferSize: 8192, // 8k buffer
        format: inputFormat) { buffer, time in
            // Analyze the current audio buffer.
            self.analysisQueue.async {
                self.streamAnalyzer.analyze(buffer, atAudioFramePosition: time.sampleTime)
            }
        }
//        self.startAudioEngine()
        print("Result....")
        
        label.text = resultsObserver.ris
        print(resultsObserver.ris)
        

    }
    

    @IBAction func buttonSha(_ sender: Any) {
        createPulse()
        self.startAudioEngine()
        
        
    }




    func createPulse() {
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: UIScreen.main.bounds.size.width/2.0, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
            let pulseLayer = CAShapeLayer()
            pulseLayer.path = circularPath.cgPath
            pulseLayer.lineWidth = 2.0
            pulseLayer.fillColor = UIColor.clear.cgColor
            pulseLayer.lineCap = CAShapeLayerLineCap.round
            pulseLayer.position = CGPoint(x: imgvAvatar.frame.size.width/2.0, y: imgvAvatar.frame.size.width/2.0)
            imgvAvatar.layer.addSublayer(pulseLayer)
            pulseLayers.append(pulseLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.animatePulse(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.animatePulse(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.animatePulse(index: 2)
                }
            }
        }
    }
    
    func animatePulse(index: Int) {
        pulseLayers[index].strokeColor = UIColor.black.cgColor

        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 2.0
        scaleAnimation.fromValue = 0.2
        scaleAnimation.toValue = 0.9
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(scaleAnimation, forKey: "scale")
        
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.duration = 2.0
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        opacityAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(opacityAnimation, forKey: "opacity")

        
    }

   // Function to Start Audio Engine for Recording Audio
   func startAudioEngine() {
       do {
           // Start the stream of audio data.
           try self.audioEngine.start()
       } catch {
           print("Unable to start AVAudioEngine: \(error.localizedDescription)")
       }
   }
    
    
}

       

    // Observer object that is called as analysis results are found.
class ResultsObserver : NSObject, SNResultsObserving {

public var ris = ""
func request(_ request: SNRequest, didProduce result: SNResult){
            
            // Get the top classification.
            guard let result = result as? SNClassificationResult,
                let classification = result.classifications.first else { return }
            
            // Determine the time of this result.
            let formattedTime = String(format: "%.2f", result.timeRange.start.seconds)
            print("Analysis result for audio at time: \(formattedTime)")
            
            let confidence = classification.confidence * 100.0
            let percent = String(format: "%.2f%%", confidence)

            // Print the result as Instrument: percentage confidence.
            print("\(classification.identifier): \(percent) confidence.\n")
             ris = classification.identifier
           
            
        }
        
        func request(_ request: SNRequest, didFailWithError error: Error) {
            print("The the analysis failed: \(error.localizedDescription)")
        }
        
        func requestDidComplete(_ request: SNRequest) {
            print("The request completed successfully!")
        }
    }


